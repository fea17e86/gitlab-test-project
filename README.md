Gitlab Howto
============


Erstelle einen Gitlab Account
-----------------------------

... auf https://about.gitlab.com/gitlab-com/


Installiere Git
---------------

1. Downloade und installiere [Git](https://git-scm.com/), du kannst dir auch einen [GUI Client](https://git-scm.com/downloads/guis) installieren.
2. Öffne Git Bash (sollte im Windows Startmenü zu finden sein) und gereriere einen [SSH Schlüssel](https://gitlab.com/help/ssh/README) mit dem Befehl `ssh-keygen -t rsa -C "your.email@example.com" -b 4096`.
3. In deinem Benutzerverzeichnis sollte nun eine Datei `id_rsa.pub` liegen, öffne sie in einem Texteditor und kopiere den gesamten Inhalt.
![SSH Schlüssel zu GitLab hinzufügen](add-ssh-key.png)


Arbeiten mit Git
----------------

1. Navigiere zu einem [Projekt auf GitLab](https://gitlab.com/fea17e86/gitlab-test-project).
2. Kopiere den Git Link.
![Kopiere den Git Link auf einer GitLab Projekt Seite](copy-git-link.png)
3. Öffne die Git Bash oder eine andere Kommandozeilenumgebung und navigiere zu einem Ordner in welches du das Projekt [`clone`n][git clone] willst.
4. Mit `git clone git@gitlab.com:fea17e86/gitlab-test-project.git` erzeugst du den Ordner `gitlab-test-project` und [`pull`st][git pull] den Inhalt des `master` branches.
5. Ändere, lösche oder füge neue Dateien hinzu.
6. Navigiere in den Ordner `gitlab-test-project` und führe `git add .` aus, um alle Änderungen für den nächsten [`commit`][git commit] zu übernehmen.
7. Führe `git commit -m "commit message"` aus, um die hinzugefügten Änderungen ins lokalen Repository mit einer [message (-m)](https://git-scm.com/docs/git-commit#git-commit--mltmsggt) zu [`commit`en][git commit].
8. Mit `git push -u origin master` [`push`st][git push] du die gemachten [`commit`s][git commit] in das remote Repository (`origin`) in den branch `master`.
  * Bevor du [`push`st][git push], solltest du immer ein `git pull` ausführen, damit du den aktuellen Stand vom remote Repository in deinen aktuellen Stand (working tree) übernimmst. Tust du das nicht, und jemand anderes hat bereits [`commit`s][git commit] [ge`push`t][git push], verweigert git deinen [`push`][git push] und informiert dich darüber.


Markdown
--------

Du kannst dich an diesem Dokument orientieren um Markdown Dokumente anzulegen, aber es lohnt sich immer ein [Markdown cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) bereit zu halten. Übrigens, Markdown Dokumente kann man auch direkt online editieren. :D


GitLab Werkzeuge
----------------

Im Projekt auf GitLab kannst du nicht nur auf die Dateien des Projekts zugreifen, sondern auch beobachten, wer, wann, welche Änderungen vorgenommen hat und diese auch kommentieren. Dies tust du unter [Repository\Commits](https://gitlab.com/fea17e86/gitlab-test-project/commits/master). Unter [Issues\Boards](https://gitlab.com/fea17e86/gitlab-test-project/boards?=) kannst du Aufgaben anlegen und sie verwalten. Wir haben sogar ein [Wiki](https://gitlab.com/fea17e86/gitlab-test-project/wikis/home).
![Kommentiere eine Änderung in einem commit auf GitLab](gitlab-comment-commit.png)
Außerdem kann man [Direktlinks auf Zeilen in einer Datei](https://gitlab.com/fea17e86/gitlab-test-project/blob/master/README.md#L39) erzeugen, indem man in der Quelltextansicht, mit der Maus über eine Zeilennummer fährt und klickt.

![Gitlab Link auf Zeile](gitlab-link-file.png)


Referenzen
----------

[git clone]
[git pull]
[git add]
[git commit]
[git push]

[git clone]: https://git-scm.com/docs/git-clone
[git pull]: https://git-scm.com/docs/git-pull
[git add]: https://git-scm.com/docs/git-add
[git commit]: https://git-scm.com/docs/git-commit
[git push]: https://git-scm.com/docs/git-push